class LoginModel {
  final String? empid;
  final String? password;

  LoginModel({
    this.empid,
    this.password,
  });

  Map<String, dynamic> toJson() {
    return {
      'empid': empid,
      'password': password,
    };
  }

  LoginModel copyWith({
    String? email,
    String? password,
  }) =>
      LoginModel(
        empid: email ?? this.empid,
        password: password ?? this.password,
      );

}


