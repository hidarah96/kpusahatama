import 'package:kpusahatama/models/LoginModel.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kpusahatama/models/LoginResultModel.dart';

import 'package:kpusahatama/services/auth_services.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc() : super(AuthInitial()) {
    on<AuthEvent>((event, emit) async {

      if (event is AuthLogin) {
        try {
          print('auth login');
          print(event.data.empid);
          print(event.data.password);

          final res = await AuthService().login(event.data);

          emit(AuthSuccess(res));
        } catch (e) {
          print(e.toString());
          emit(AuthFailed(e.toString()));
        }
      }

    });
  }
}