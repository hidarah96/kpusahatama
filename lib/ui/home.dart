import 'package:flutter/material.dart';
import 'package:kpusahatama/Utilities/theme.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        color: Colors.white,
        child: ListView(
          padding: const EdgeInsets.only(top: 50),
          children: [
            profileCard()
          ],
        )
      ),
    );
  }
}

Widget profileCard() {
  return Container(
    padding: EdgeInsets.only(top: 30, left: 40, right: 40),
    child: Card(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            width: 150,
            height: 50,
            margin: const EdgeInsets.only(
                top: 0
            ), decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('images/icLogo.png'))
          ),
          ),
        ],
      ),
    ),
  );
}

// ListTile(
// title: Text('KPUSahatama'),
// subtitle: Text('Memahami dan Melayani'),
// ),