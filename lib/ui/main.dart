import 'package:kpusahatama/bloc/login/auth_bloc.dart';
import 'package:kpusahatama/models/LoginModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kpusahatama/ui/home.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => AuthBloc(),
      child: MaterialApp(
        title: 'Login',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(primaryColor: Colors.black87),
        home: Login(),
      ),
    );
  }
}

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  String username = "hidarah96";
  String password = "Rahmadkpusahatama";
  String alert = "siap login";

  TextEditingController usernameInput = TextEditingController(text: '');
  TextEditingController passwordInput = TextEditingController(text: '');

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthBloc, AuthState>(
      listener: (context, state) {
        if (state is AuthSuccess) {
          print('xxx login welcome');
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => Home())
          );
        }

        if (state is AuthFailed) {
          print('xxx masuk failed');
        }
      },
      builder: (context, state) {
        return Scaffold(
          body: Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.all(18),
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 150,
                  height: 50,
                margin: const EdgeInsets.only(
                  top: 0
                ), decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('images/icLogo.png'))
                ),
                ),
                const SizedBox(
                  height: 20,
                ),
                const Text(
                  "Selamat Datang, Silahkan Masuk",
                  style: TextStyle(fontSize: 20, color: Colors.black87),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextField(
                  controller: usernameInput,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black87)),
                      prefixIcon: Icon(
                        Icons.person,
                        size: 40,
                      ),
                      hintText: "Masukan Username",
                      hintStyle: TextStyle(color: Colors.black87),
                      labelText: "Username",
                      labelStyle: TextStyle(color: Colors.black87)),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextField(
                  obscureText: true,
                  controller: passwordInput,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black87)),
                      prefixIcon: Icon(
                        Icons.lock,
                        size: 40,
                      ),
                      hintText: "Masukan Password",
                      hintStyle: TextStyle(color: Colors.black87),
                      labelText: "Password",
                      labelStyle: TextStyle(color: Colors.black87)),
                ),
                const SizedBox(
                  height: 20,
                ),
                Card(
                  color: Colors.black87,
                  elevation: 5,
                  child: Container(
                    height: 50,
                    child: InkWell(
                      splashColor: Colors.white,
                      onTap: () {
                        context.read<AuthBloc>().add(
                          AuthLogin(
                            LoginModel(
                              empid: usernameInput.text,
                              password: passwordInput.text,
                            ),
                          ),
                        );
                        print('ui');
                        print(usernameInput.text);
                        print(passwordInput.text);
                      },
                      child: const Center(
                        child: Text(
                          "Masuk",
                          style: TextStyle(fontSize: 20, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
