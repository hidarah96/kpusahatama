import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:kpusahatama/models/LoginResultModel.dart';
import 'package:kpusahatama/models/LoginModel.dart';

class AuthService {
  final String baseUrl = 'http://portalapi.kpusahatama.id';

  Future<LoginResultModel> login(LoginModel data) async {
    try {
      final res = await http.post(
        Uri.parse(
          '$baseUrl/api/login',
        ),
        body: data.toJson(),
      );

      print(res.body);

      if (res.statusCode == 200 || res.statusCode == 201) {
        final user = LoginResultModel.fromJson(jsonDecode(res.body));
        // user.password = data.password;

        // await storeCredentialToLocal(user);

        return user;
      } else {
        throw jsonDecode(res.body)['message'];
      }
    } catch (e) {
      print(e);
      rethrow;
    }
  }
}